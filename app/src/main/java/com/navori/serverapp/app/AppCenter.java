package com.navori.serverapp.app;

import android.app.Application;

import com.sjl.foreground.Foreground;


public class AppCenter extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        Foreground.init(this);
    }

}
