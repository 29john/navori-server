package com.navori.serverapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.navori.serverapp.service.ConnectService;

/**
 * Created by Wildnet Technologies on 7/11/2017.
 */

public class AutoStartReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent arg1) {
        Intent intent = new Intent(context,ConnectService.class);
        context.startService(intent);
        Log.i("Autostart", "started");
    }
}
