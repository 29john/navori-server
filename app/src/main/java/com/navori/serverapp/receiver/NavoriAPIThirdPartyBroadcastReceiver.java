package com.navori.serverapp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;


public class NavoriAPIThirdPartyBroadcastReceiver extends BroadcastReceiver {



	/*****************************************************
	 * interface : OnDataIsReadyListener
	 *
	 * The third party application must implement the OnDataIsReadyListener interface.
	 * DataIsReady() is called when the data is completely received from the Engine.
	 * From there all the data and API functions are available
	 *
	 */
	public interface OnDataIsReadyListener
	{
		public void DataIsReady();
	}

	/*****************************************************
	 * interface : OnMediaIsReadyListener
	 *
	 * The third party application must implement the OnMediaIsReadyListener interface.
	 * MediaIsReady() is called when the Media is started from the Engine.
	 *
	 */
	public interface OnMediaIsReadyListener
	{
		public void MediaIsReady(Bundle bundle);
	}

	/*****************************************************
	 * field : context
	 *
	 * The context will be used to register the application as an intent receiver
	 * and to broadcast intents to the Navori Engine.
	 */
	static Context context;

	/*****************************************************
	 * field : dataIsReadyListener
	 *
	 * The dataIsReadyListener will be used to notify the third party application
	 * that the data is ready to use.
	 */
	static OnDataIsReadyListener dataIsReadyListener;


	/*****************************************************
	 * field : mediaIsReadyListener
	 *
	 * The mediaIsReadyListener will be used to notify the third party application
	 * that the media is ready to play.
	 */
	static OnMediaIsReadyListener mediaIsReadyListener;

	/*****************************************************
	 * function : Init
	 *
	 * The init function gets the application context in parameter.
	 * The context will be used to register the application as an intent receiver
	 * and to broadcast intents to the Navori Engine.
	 *
	 ******************************************************/
	public void Init(Context _context, OnDataIsReadyListener _dataIsReadyListener,OnMediaIsReadyListener _mediaIsReadyListener)
	{
		context = _context;
		dataIsReadyListener = _dataIsReadyListener;
		mediaIsReadyListener = _mediaIsReadyListener;
		IntentFilter APIIntentFilter = new IntentFilter(NavoriAPIThirdPartyBroadcastReceiver.THIRD_PARTY_RECEIVER);
		context.registerReceiver(this, APIIntentFilter);
		SynchronizeData();
	}

	/*****************************************************
	 * function : InitUnregister Broadcast Receiver
	 *
	 * The initUnregister function unregister register broadcast.

	 *
	 ******************************************************/
	public void InitUnregister() {
		context.unregisterReceiver(this);
	}


	/*****************************************************
	 * function : Free
	 *
	 * Here we need to free all the used resources.
	 *
	 ******************************************************/
	public void Free()
	{
		context = null;
	}

	/*****************************************************
	 *
	 * 			RECEIVE FUNCTIONS
	 *
	 ******************************************************/

	/*****************************************************
	 * field : THIRD_PARTY_RECEIVER
	 *
	 * The application need to listen to this "action". This is the "action"
	 * the Navori Engine uses to send intents to the third party application
	 */
	static final String THIRD_PARTY_RECEIVER = "navori.api.thirdparty";


	/*****************************************************
	 * field : data arrays
	 *
	 * We need arrays to store the lists of media and playlist..
	 */
	public long[] MediaIdArray = null;
	public String[] MediaNameArray = null;
	public int[] MediaTypeArray = null;
	public long[] MediaDurationArray = null;
	public boolean[] MediaIsTriggerArray = null;
	public long[] PlaylistIdArray = null;
	public String[] PlaylistNameArray = null;
	public long[] PlaylistDurationArray = null;

	public Boolean TriggerAddonIsActivated = false;


	/*****************************************************
	 * function : onReceive
	 *
	 * Here we receive intents from the Navori Engine.
	 *
	 ******************************************************/
	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle extras = intent.getExtras();
		if (extras != null) {
			String state = extras.getString("function");

			if (state != null && state.equals(SEND_DATA_TO_THIRD_PART)) {
				SendDataToThirdPart(extras);
			} else {
				Boolean isReceipt = extras.getBoolean("isReceipt");
				if (isReceipt)
					SendTriggerReceipt(extras);
			}

		}
	}


	/*****************************************************
	 * field : SEND_DATA_TO_THIRD_PART
	 *
	 * This is the only function we need to handle.
	 * The Navori Engine call this function when the data are ready or when the data
	 * have just been updated.
	 */
	public final static String SEND_DATA_TO_THIRD_PART = "SendDataToThirdPart";
	public final static String SEND_TRIGGER_RECEIPT = "SendTriggerReceipt";

	/*****************************************************
	 * function : SendDataToThirdPart
	 *
	 * The Navori Engine calls this function when the data are ready or when the data
	 * have just been updated.
	 *
	 ******************************************************/
	private void SendDataToThirdPart(Bundle bundle) {
		TriggerAddonIsActivated = bundle.getBoolean("TriggerAddonIsActivated");

		if (TriggerAddonIsActivated) {
			MediaIdArray = bundle.getLongArray("MediaIdArray");
			MediaNameArray = bundle.getStringArray("MediaNameArray");
			MediaTypeArray = bundle.getIntArray("MediaTypeArray");
			MediaDurationArray = bundle.getLongArray("MediaDurationArray");
			MediaIsTriggerArray = bundle.getBooleanArray("MediaIsTriggerArray");
			PlaylistIdArray = bundle.getLongArray("PlaylistIdArray");
			PlaylistNameArray = bundle.getStringArray("PlaylistNameArray");
			PlaylistDurationArray = bundle.getLongArray("PlaylistDurationArray");

			dataIsReadyListener.DataIsReady();
		}
	}

	/*****************************************************
	 * function : SendTriggerReceipt
	 *
	 * The Navori Engine calls this function after every tim a trigger function has been called.
	 * The parameter is the exact bundle that has been sent to the engine so the third party application can
	 * identify which command has been received.
	 *
	 ******************************************************/
	private void SendTriggerReceipt(Bundle bundle) {
		mediaIsReadyListener.MediaIsReady( bundle);
	}

	/*****************************************************
	 *
	 * 			SEND FUNCTIONS
	 *
	 ******************************************************/

	//Global Variables
	/*****************************************************
	 * field : NAVORI_ENGINE_RECEIVER
	 *
	 * This is the "action" the Navori Engine listens to.
	 */
	public final static String NAVORI_ENGINE_RECEIVER = "navori.api.receiver";


	/*****************************************************
	 * field : functions' names
	 *
	 * Those are the functions the Navori Engine can handle.
	 */
	public final static String SYNCHRONIZE_DATA = "SynchronizeData";
	public final static String PLAY_MEDIA = "PlayMedia";
	public final static String PLAY_MEDIA_LIST = "PlayMediaList";
	public final static String PLAY_PLAYLIST = "PlayPlaylist";
	public final static String PLAY_WEB_URL = "PlayWebUrl";
	public final static String PLAY_STREAMING_URL = "PlayStreamingUrl";
	public final static String RELEASE = "Release";
	public final static String CLOSE_ENGINE = "CloseEngine";

	/*****************************************************
	 * function : SynchronizeData
	 *
	 * Requests for an update of the data,
	 * the response will be by calling the "SEND_DATA_TO_THIRD_PART" function.
	 *
	 ******************************************************/
	public void SynchronizeData() {
		Bundle extras = new Bundle();
		extras.putString("function", SYNCHRONIZE_DATA);

		Intent intent = new Intent();
		intent.putExtras(extras);

		intent.setAction(NAVORI_ENGINE_RECEIVER);
		context.sendBroadcast(intent);
	}

	/*****************************************************
	 * function : PlayMedia
	 *
	 * Requests to play "nbLoop" loops the media.
	 * Set "nbLoop" to 0 for an infinite playback that will end with the call of Release().
	 *
	 ******************************************************/
	public void PlayMedia(long MediaId, int MediaType, int nbLoop) {
		Bundle extras = new Bundle();
		extras.putString("function", PLAY_MEDIA);

		extras.putLong("MediaId", MediaId);
		extras.putInt("MediaType", MediaType);
		extras.putInt("nbLoop", nbLoop);

		Intent intent = new Intent();
		intent.putExtras(extras);

		intent.setAction(NAVORI_ENGINE_RECEIVER);
		context.sendBroadcast(intent);
	}



	/*****************************************************
	 * function : PlayMediaList
	 *
	 * Requests to play "nbLoop" loops the playlist previously formed by the media contained in the "MediaIdList" array.
	 * Set "nbLoop" to 0 for an infinite playback that will end with the call of Release().
	 *
	 ******************************************************/
	public void PlayMediaList(long[] MediaIdList, int[] MediaTypeList, int nbLoop) {
		Bundle extras = new Bundle();
		extras.putString("function", PLAY_MEDIA_LIST);

		extras.putLongArray("MediaIdList", MediaIdList);
		extras.putIntArray("MediaTypeList", MediaTypeList);
		extras.putInt("nbLoop", nbLoop);

		Intent intent = new Intent();
		intent.putExtras(extras);

		intent.setAction(NAVORI_ENGINE_RECEIVER);
		context.sendBroadcast(intent);
	}

	/*****************************************************
	 * function : PlayPlaylist
	 *
	 * Requests to play "nbLoop" loops the playlist.
	 * Set "nbLoop" to 0 for an infinite playback that will end with the call of Release().
	 *
	 ******************************************************/
	public void PlayPlaylist(long PlaylistId, int nbLoop) {
		Bundle extras = new Bundle();
		extras.putString("function", PLAY_PLAYLIST);

		extras.putLong("PlaylistId", PlaylistId);
		extras.putInt("nbLoop", nbLoop);

		Intent intent = new Intent();
		intent.putExtras(extras);

		intent.setAction(NAVORI_ENGINE_RECEIVER);
		context.sendBroadcast(intent);
	}

	/*****************************************************
	 * function : PlayWebUrl
	 *
	 * Requests to play "nbLoop" loops the web URL "url" for a duration of "duration" ms each loop.
	 * Set "nbLoop" to 0 for an infinite playback that will end with the call of Release().
	 *
	 ******************************************************/
	public void PlayWebUrl(String url, long duration, int nbLoop) {
		Bundle extras = new Bundle();
		extras.putString("function", PLAY_WEB_URL);

		extras.putString("url", url);
		extras.putLong("duration", duration);
		extras.putInt("nbLoop", nbLoop);

		Intent intent = new Intent();
		intent.putExtras(extras);

		intent.setAction(NAVORI_ENGINE_RECEIVER);
		context.sendBroadcast(intent);
	}

	/*****************************************************
	 * function : PlayStreamingUrl
	 *
	 * Requests to play "nbLoop" loops the streaming video whose URL is "url" for a duration of "duration" ms each loop.
	 * Set "nbLoop" to 0 for an infinite playback that will end with the call of Release().
	 *
	 ******************************************************/
	public void PlayStreamingUrl(String url, long duration, int nbLoop) {
		Bundle extras = new Bundle();
		extras.putString("function", PLAY_STREAMING_URL);

		extras.putString("url", url);
		extras.putLong("duration", duration);
		extras.putInt("nbLoop", nbLoop);

		Intent intent = new Intent();
		intent.putExtras(extras);

		intent.setAction(NAVORI_ENGINE_RECEIVER);
		context.sendBroadcast(intent);
	}

	/*****************************************************
	 * function : Release
	 *
	 * Frees the player which takes up the playback of the scheduled playlist at the media
	 * where it has been stopped (even if a number of loop has been set).
	 *
	 ******************************************************/
	public void Release() {
		Bundle extras = new Bundle();
		extras.putString("function", RELEASE);

		Intent intent = new Intent();
		intent.putExtras(extras);

		intent.setAction(NAVORI_ENGINE_RECEIVER);
		context.sendBroadcast(intent);
	}

	/*****************************************************
	 * function : CloseEngine
	 *
	 * Closes the player, the Conductor will not restart the engine.
	 *
	 ******************************************************/
	public void CloseEngine() {
		Bundle extras = new Bundle();
		extras.putString("function", CLOSE_ENGINE);

		Intent intent = new Intent();
		intent.putExtras(extras);

		intent.setAction(NAVORI_ENGINE_RECEIVER);
		context.sendBroadcast(intent);
	}

}
