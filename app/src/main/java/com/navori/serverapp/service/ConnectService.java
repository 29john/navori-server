package com.navori.serverapp.service;


import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.navori.serverapp.R;
import com.navori.serverapp.receiver.NavoriAPIThirdPartyBroadcastReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Wildnet Technologies on 7/11/2017.
 */

public class ConnectService extends Service implements NavoriAPIThirdPartyBroadcastReceiver.OnDataIsReadyListener,NavoriAPIThirdPartyBroadcastReceiver.OnMediaIsReadyListener {

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_ERROR = 6;
    public static final int MESSAGE_LOST = 7;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;


    // Name of the connected device
    private String mConnectedDeviceName = null;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;

    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;

    private NavoriAPIThirdPartyBroadcastReceiver navoriAPIBDemoBroadcastReceiver;
    // Member object for the chat services
    public BluetoothChatService mChatService = null;

    Timer testAPITimer = null;
    private Timer myTimer;
    Toast toast = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("TAG", "Service started.");
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //Create and initialize the broadcast receiver
        navoriAPIBDemoBroadcastReceiver = new NavoriAPIThirdPartyBroadcastReceiver();
        navoriAPIBDemoBroadcastReceiver.Init(this /*context*/, this /*OnDataIsReadyListener*/, this /*OnMediaIsReadyListener*/);


        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {

            showToast("Bluetooth is not available");
            stopSelf();

        }

        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
            startActivity(discoverableIntent);
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Log.d("BT SERVICE", "BLUETOOTH NOT ON, STOPPING SERVICE");
            stopSelf();
        } else {
            if (mChatService == null) setupChat();
        }

        if (mChatService != null) {
            if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
                mChatService.start();
                Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage("com.navori.engine");
                LaunchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(LaunchIntent);
            }
        }
        return START_STICKY;
    }


    private void setupChat() {
        // Initialize the BluetoothChatService to perform bluetooth connections

        mChatService = new BluetoothChatService(this, mHandler);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatService != null) mChatService.stop();
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    if(readMessage.startsWith("Content")) {
                        showToast("Content: " + readMessage.split("&@#¶")[3]);
                        navoriAPIBDemoBroadcastReceiver.PlayMedia(Long.parseLong(readMessage.split("&@#¶")[1]), Integer.parseInt(readMessage.split("&@#¶")[2]), Integer.parseInt(readMessage.split("&@#¶")[4]));
                    }else if(readMessage.startsWith("Trigger")) {
                        showToast("Trigger: " + readMessage.split("&@#¶")[3]);
                        navoriAPIBDemoBroadcastReceiver.PlayMedia(Long.parseLong(readMessage.split("&@#¶")[1]), Integer.parseInt(readMessage.split("&@#¶")[2]), Integer.parseInt(readMessage.split("&@#¶")[4]));
                    } else if (readMessage.equalsIgnoreCase("hi")) {
                        sendIntialDataToClient();
                    } else if (readMessage.startsWith("PlayList")) {
                        showToast("Playlist: "+readMessage.split("&@#¶")[2]);
                        navoriAPIBDemoBroadcastReceiver.PlayPlaylist(Long.parseLong(readMessage.split("&@#¶")[1]), Integer.parseInt(readMessage.split("&@#¶")[3]));
                    } else if (readMessage.contains("release")) {
                        showToast(readMessage);
                        navoriAPIBDemoBroadcastReceiver.Release();
                    }

                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    showToast("Connected to " + mConnectedDeviceName);
                    Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage("com.navori.engine");
                    LaunchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(LaunchIntent);

                    myTimer = new Timer();
                    myTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if (navoriAPIBDemoBroadcastReceiver.MediaIdArray == null || navoriAPIBDemoBroadcastReceiver.MediaIdArray.length == 0) {
                                showToast("Trigger Media Add-on is not activated on the player license");
                                myTimer.cancel();
                                String str = "NoTriggerAddOn";
                                byte[] send = new byte[0];
                                try {
                                    send = str.getBytes("UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                mChatService.write(send);
                            } else {
                                sendIntialDataToClient();
                                myTimer.cancel();
                            }
                        }
                    }, 0, 5000);

                    break;
                case MESSAGE_TOAST:

                    showToast(msg.getData().getString(TOAST));
                    navoriAPIBDemoBroadcastReceiver.InitUnregister();
                    stopSelf();
                    startService(new Intent(ConnectService.this, ConnectService.class));
                    break;

            }
        }
    };


    private void sendFinalMsg(String str) {
        byte[] send = new byte[0];
        try {
            send = str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        mChatService.write(send);
//        if (str.length() > 200) {
//            final ArrayList<String> strings = splitStringBySize(str, 200);
//            for (int i = 0; i < strings.size(); i++) {
//                final int[] j = new int[1];
//                j[0] = i;
//                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        String str1;
//                        str1 = strings.get(j[0]) + "&&&Navori";
//                        if (j[0] == strings.size() - 1)
//                            str1 = strings.get(j[0]) + "&&&Last";
//                        Log.e("chunk", str1);
//                        byte[] send = str1.getBytes();
//                        mChatService.write(send);
//                    }
//                }, 500);
//            }
//        } else {
//            str= str + "&&&Normal";
//            byte[] send = str.getBytes();
//            mChatService.write(send);
//        }
    }


    private ArrayList<String> splitStringBySize(String str, int size) {
        ArrayList<String> split = new ArrayList<>();
        for (int i = 0; i <= str.length() / size; i++) {
            split.add(str.substring(i * size, Math.min((i + 1) * size, str.length())));
        }
        return split;
    }

    private void showToast( String msg) {
       String temp ="";
        if(msg.length()<36){
            temp = msg;
        }else{
            temp =  msg.substring(0, 32) +"...";
        }
        final  String text = temp;
        final Thread thread = new Thread() {
            public void run() {
                Looper.prepare();
                toast = null;
                if (toast == null) {
                    toast = Toast.makeText( ConnectService.this, text, Toast.LENGTH_SHORT);
                }
                if (!toast.getView().isShown()) {
                    LinearLayout toastLayout = (LinearLayout) toast.getView();
                    TextView toastTV = (TextView) toastLayout.getChildAt(0);
                    toastTV.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light_1.ttf"));
                    toastTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, ConnectService.this.getResources().getDimension(R.dimen._9sdp));
                    toastTV.setTextColor(Color.WHITE);
                     toast.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);
                    toast.getView().setBackgroundColor(Color.TRANSPARENT);
                    toast.show();
                }

               Looper.loop();
            }
        };
        thread.start();
    }

    private void sendIntialDataToClient() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (navoriAPIBDemoBroadcastReceiver.MediaIdArray != null && navoriAPIBDemoBroadcastReceiver.MediaIdArray.length > 0) {
            String str = convertArrayListInJsonArray();
            byte[] send = new byte[0];
            try {
                send = str.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            mChatService.write(send);

        }
    }


    private String convertArrayListInJsonArray() {
        String jsonStr = "";
        JSONObject contactsObj = new JSONObject();

        JSONArray idsArray = new JSONArray(), typesArray = new JSONArray(), nameArray = new JSONArray(), triggerArray = new JSONArray(), durationArray = new JSONArray();
        JSONArray playIdsArray = new JSONArray(), playNameArray = new JSONArray(), playDurationArray = new JSONArray();
        try {
            if (navoriAPIBDemoBroadcastReceiver.MediaIdArray != null && navoriAPIBDemoBroadcastReceiver.MediaIdArray.length > 0)
                idsArray = new JSONArray(Arrays.asList(navoriAPIBDemoBroadcastReceiver.MediaIdArray));
            if (navoriAPIBDemoBroadcastReceiver.MediaTypeArray != null && navoriAPIBDemoBroadcastReceiver.MediaTypeArray.length > 0)
                typesArray = new JSONArray(Arrays.asList(navoriAPIBDemoBroadcastReceiver.MediaTypeArray));
            if (navoriAPIBDemoBroadcastReceiver.MediaNameArray != null && navoriAPIBDemoBroadcastReceiver.MediaNameArray.length > 0)
                nameArray = new JSONArray(Arrays.asList(navoriAPIBDemoBroadcastReceiver.MediaNameArray));
            if (navoriAPIBDemoBroadcastReceiver.MediaIsTriggerArray != null && navoriAPIBDemoBroadcastReceiver.MediaIsTriggerArray.length > 0)
                triggerArray = new JSONArray(Arrays.asList(navoriAPIBDemoBroadcastReceiver.MediaIsTriggerArray));
            if (navoriAPIBDemoBroadcastReceiver.MediaDurationArray != null && navoriAPIBDemoBroadcastReceiver.MediaDurationArray.length > 0)
                durationArray = new JSONArray(Arrays.asList(navoriAPIBDemoBroadcastReceiver.MediaDurationArray));

            if (navoriAPIBDemoBroadcastReceiver.PlaylistIdArray != null && navoriAPIBDemoBroadcastReceiver.PlaylistIdArray.length > 0)
                playIdsArray = new JSONArray(Arrays.asList(navoriAPIBDemoBroadcastReceiver.PlaylistIdArray));
            if (navoriAPIBDemoBroadcastReceiver.PlaylistDurationArray != null && navoriAPIBDemoBroadcastReceiver.PlaylistDurationArray.length > 0)
                playDurationArray = new JSONArray(Arrays.asList(navoriAPIBDemoBroadcastReceiver.PlaylistDurationArray));
            if (navoriAPIBDemoBroadcastReceiver.PlaylistNameArray != null && navoriAPIBDemoBroadcastReceiver.PlaylistNameArray.length > 0)
                playNameArray = new JSONArray(Arrays.asList(navoriAPIBDemoBroadcastReceiver.PlaylistNameArray));


            contactsObj.put("ids", idsArray);
            contactsObj.put("types", typesArray);
            contactsObj.put("names", nameArray);
            contactsObj.put("trigger", triggerArray);
            contactsObj.put("duration", durationArray);
            contactsObj.put("playId", playIdsArray);
            contactsObj.put("playName", playNameArray);
            contactsObj.put("playDuration", playDurationArray);

        } catch (JSONException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        jsonStr = contactsObj.toString();
        Log.e("response!", jsonStr);

        return jsonStr;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void DataIsReady() {

        sendIntialDataToClient();
    }

    @Override
    public void MediaIsReady(Bundle bundle) {
        String  str ="";
        if(bundle.getString("function").equalsIgnoreCase("PlayMedia")){
            str = ""+ bundle.getLong("MediaId")+"&@#¶"+bundle.getInt("nbLoop") +"&@#¶"+ bundle.getInt("MediaType") ;
        }else if(bundle.getString("function").equalsIgnoreCase("PlayPlaylist")){
            str = ""+ bundle.getLong("PlaylistId")+"&@#¶"+bundle.getInt("nbLoop") ;
        }
        byte[] send = new byte[0];
        try {
            send = str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        mChatService.write(send);
    }
}
