package com.navori.serverapp.activity;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.navori.serverapp.R;
import com.navori.serverapp.service.ConnectService;

import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity{

    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    private Toast m_currentToast;
    private static final int REQUEST_DISCOVERABLE_BT = 3;
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_ERROR = 6;
    public static final int MESSAGE_LOST = 7;

    // Key names received from the BluetoothChatService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private boolean isStart = false;
    private     Toast toast = null;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client_activity);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {

            showToast("Bluetooth is not available");
            finish();
            return;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
       makeDiscoverable();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    startService();
                } else {
                    // User did not enable Bluetooth or an error occured
                    showToast(getResources().getString(R.string.bt_not_enabled_leaving));
                    finish();
                }
                break;
            case REQUEST_DISCOVERABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    startService();
                } else {
                    // User did not enable Bluetooth or an error occured
                   makeDiscoverable();
                }
                break;
        }
    }

    private void makeDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 0);
            startActivityForResult(discoverableIntent,REQUEST_DISCOVERABLE_BT);
        }else{
            isStart = true;
            startService();
        }

    }

    private void startService() {
        if(isStart) {
            Intent intent = new Intent(this, ConnectService.class);
            startService(intent);
            Log.i("Autostart", "started");
        }

    }

    void showToast(String msg) {
        String temp ="";
        if(msg.length()<36){
            temp = msg;
        }else{
            temp =  msg.substring(0, 32) +"...";
        }
        final  String text = temp;
        Toast toast = null;
        if (toast == null) {
            toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
        }
        LinearLayout toastLayout = (LinearLayout) toast.getView();
        TextView toastTV = (TextView) toastLayout.getChildAt(0);
        toastTV.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light_1.ttf"));
        toastTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getResources().getDimension(R.dimen._9sdp));
        toastTV.setTextColor(Color.WHITE);
        toastTV.setMaxLines(1);
         toast.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);
        toast.getView().setBackgroundColor(Color.TRANSPARENT);
        toast.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
